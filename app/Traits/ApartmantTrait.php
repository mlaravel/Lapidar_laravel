<?php

namespace App\Traits;

use App\Models\Apartment;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\CustomException;


trait ApartmentTrait
{
    public function withTrait() : Collection {
        return Apartment::with('rooms')->get();
    }

    public function testarExcecao() {
        throw new ModelNotFoundException();
    }

    public function testarCustomExcecao() {
        throw new CustomException('Exceção customizada funcionando.');
    }
}
