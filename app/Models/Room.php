<?php

namespace App\Models;

use App\Apartment;
use App\Models\GameRoom;
use App\Models\DinnerRoom;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $primaryKey = 'cd_room';
    protected $fillable = [
        'windows', 'door', 'cd_apartment'
    ];
    public $timestamps = false;

    public function apartment() {
        return $this->belongsTo(Apartment::class ,'cd_apartment');
    }

    public function dinnerRoom() {
        return $this->hasOne(DinnerRoom::class, 'cd_room')
            ->withDefault(function($q) {
                return $this->morphTo();
            });
    }

    public function gameRoom() {
        return $this->hasOne(GameRoom::class, 'cd_room')
            ->withDefault(function($q) {
                return $this->morphTo();
            });
    }
}
