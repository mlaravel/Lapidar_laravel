<?php

namespace App\Models;

use App\Models\Room;
use Illuminate\Database\Eloquent\Model;

class GameRoom extends Room
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        parent::boot();
    }
    protected $table = "game_room";   
    protected $primaryKey = 'cd_room';
    protected $fillable = [
        'cd_room', 'console', 'quantidade'
    ];

    public $timestamps = false;

    public function room() {
        return $this->belongsTo(Room::class, 'cd_room');
    }
}
