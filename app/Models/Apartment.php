<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    protected $primaryKey = 'cd_apartment';
    protected $fillable = [
        'number', 'floor'
    ];
    public $timestamps = false;

    public function users() {
        return $this->belongsToMany(User::class, 'cd_user_apartment', 'cd_user', 'cd_apartment');
    }

    public function rooms() {
        return $this->hasMany(Room::class, 'cd_apartment');
    }
}
