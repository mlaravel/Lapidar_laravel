<?php

namespace App\Models;

use App\Models\Room;
use Illuminate\Database\Eloquent\Model;

class DinnerRoom extends Room
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        parent::boot();
    }
    protected $table = "dinner_room"; 
    protected $primaryKey = 'cd_room';
    protected $fillable = [
        'cd_room', 'qtdd_tables', 'qtdd_chair'
    ];

    public $timestamps = false;

    public function room() {
        return $this->belongsTo(Room::class, 'cd_room');
    }
}
