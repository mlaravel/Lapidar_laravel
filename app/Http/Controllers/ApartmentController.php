<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApartmentTrait;

class ApartmentController extends Controller
{
    use ApartmentTrait;

    public function with() {
        return $this->withTrait();
    }

    public function notFoundException() {
        return $this->testarExcecao();
    }

    public function customException() {
        return $this->testarCustomExcecao();
    }
}
