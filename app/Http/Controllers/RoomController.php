<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RoomService;
use App\Models\Room;
use App\Models\GameRoom;
use App\Models\DinnerRoom;

class RoomController extends Controller
{
    protected $roomService;

    public function __construct(RoomService $roomService) {
        $this->roomService = $roomService;
    }

    public function listRoomWithChildren() {
        return $this->roomService->listRooms();
    }

    public function store(Request $request) {
        $type = $request->input('room')['@type'];
        $room = new Room($request->all());
        $typeRoom = $type === "dinner_room" ? new DinnerRoom($request->input('room')) : new GameRoom($request->input('room'));
        $room->type = $typeRoom;
        RoomService::save($room, $type);
        return response()->json([], 201);
    }
}
