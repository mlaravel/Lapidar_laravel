<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UsersService;

class UserController extends UsersService
{
    public function withJoin() {
        return $this->withJoinService();
    }
}
