<?php

namespace App\Services;

use App\Models\Room;
use App\Models\GameRoom;
use App\Models\DinnerRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class RoomService
{
    /**
     * Retorna todos as salas com seus "filhos".
     *
     * @return Collection
     * @author Marcelo Nascimento <marcelo.laravel@gmail.com>
     * @version 1.0.0
     */
    public function listRooms() : Collection {
        return Room::with(['dinnerRoom', 'gameRoom'])->get();
    }

    public static function save(Room $room, string $type) : void {
        DB::transaction(function () use($room, $type) {
            $type = $room->type;
            unset($room->type);
            $room = Room::create($room->toArray());
            $type->room()->associate($room);
            $type->save();
        });
    }
}
