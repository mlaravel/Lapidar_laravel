<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UsersService
{
    /**
     * Retorna todos os usuários com seus apartamentos e salas.
     *
     * @return Collection
     * @author Marcelo Nascimento <marcelo.laravel@gmail.com>
     * @version 1.0.0
     */
    public function withJoinService() : Collection {
        return User::with(['apartments' => function($q){
            $q->join('rooms', 'rooms.cd_apartment', 'apartments.cd_apartment');
        }])
        ->get();
    }
}
