<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserApartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_apartments', function (Blueprint $table) {
            $table->increments('cd_user_apartment');
            $table->integer('cd_user')->unsigned();
            $table->integer('cd_apartment')->unsigned();
            $table->foreign('cd_user')->references('cd_user')->on('users')->onDelete('cascade');
            $table->foreign('cd_apartment')->references('cd_apartment')->on('apartments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_apartments');
    }
}
