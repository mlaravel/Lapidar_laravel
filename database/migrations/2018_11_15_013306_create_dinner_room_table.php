<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDinnerRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dinner_room', function (Blueprint $table) {
            $table->integer('cd_room')->unsigned();;
            $table->foreign('cd_room')->references('cd_room')->on('rooms')->onDelete('cascade');
            $table->integer('qtdd_tables');
            $table->integer('qtdd_chair');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dinner_room');
    }
}
