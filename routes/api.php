<?php

Route::get('users/withJoin', 'UserController@withJoin');
Route::get('apartments/with', 'ApartmentController@with');
Route::get('apartments/notFoundException', 'ApartmentController@notFoundException');
Route::get('apartments/customException', 'ApartmentController@customException');
Route::get('rooms/generalization', 'RoomController@listRoomWithChildren');
Route::post('rooms/persistence', 'RoomController@store');
